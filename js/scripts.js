let cursosFake = [
  {
    id: 1,
    nombre: 'Angular 8',
    fecha: '2020-10-02'
  },
  {
    id: 2,
    nombre: 'Progressive Web Applications',
    fecha: '2020-01-17'
  },
  {
    id: 3,
    nombre: 'Control de versiones con Git',
    fecha: '2019-11-15'
  },
  {
    id: 5,
    nombre: 'JavaScript + JSON',
    fecha: '2020-02-24'
  },
  {
    id: 6,
    nombre: 'Cómo preparar un bacalao al pil pil',
    fecha: '2020-03-24'
  },
  {
    id: 7,
    nombre: 'Ser padre y no morir en el intento',
    fecha: '2020-10-01'
  },
  {
    id: 8,
    nombre: 'Aprender a poner nombres de variables',
    fecha: '2019-09-07'
  }
];

class Cursos {
  constructor() {
    this.inicializaDatepicker();
    this.asignarHTML();
    this.activaDesactivaCargando(true);
    setTimeout(() => {
      this.cargaCursos();
      this.registraListeners();
      this.pintaListaCursos();
      this.activaDesactivaCargando(false);
    }, 500);
  }

  cargaCursos() {
    fetch('http://api.cursos.com/lista').then(resp => resp.json()).then(cursos => {
      this.cursos = [...cursos];
    });
  }

  cambiaPagina(pagina) {
    for (let page of document.querySelectorAll('.page')) {
      page.classList.add('off');
    }
    document.querySelector(`.page-${pagina}`).classList.remove('off');
    if (pagina === 'form') {
      this.botonVolver.classList.remove('off');
      this.toolBar.classList.add('off');
    } else {
      this.botonVolver.classList.add('off');
      this.toolBar.classList.remove('off');
    }
  }

  registraListeners() {
    this.checkCursosFuturos.addEventListener('change', () => {
      if (this.checkCursosFuturos.checked) {
        this.cursos = this.filtraCursosFuturos();
      } else {
        this.cargaCursos();
      }
      this.pintaListaCursos();
    });
    this.botonAnyadir.addEventListener('click', () => {
      this.cambiaPagina('form');
    });
    this.botonVolver.addEventListener('click', () => {
      this.cambiaPagina('list');
    });
    this.controlNombre.addEventListener('change', () => {
      this.compruebaBotonCrear();
    });
    this.formularioCrear.addEventListener('submit', e => {
      e.preventDefault();
      if (this.validaForm()) {
        this.crearCurso();
      }
    });
  }

  crearCurso() {
    const nuevoCurso = {
      id: 999,
      nombre: this.controlNombre.value,
      fecha: moment(this.controlFecha.value, 'DD-MM-YYYY').format('YYYY-MM-DD')
    };
    fetch('http://api.cursos.com/cursos', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(nuevoCurso)
    }).then(resp => {
      this.vaciarFormulario();
      this.cargaCursos();
      this.cambiaPagina('list');
      this.muestraMensaje('Se ha creado el curso ' + this.controlNombre.value);
      this.pintaListaCursos();
    });
  }

  vaciarFormulario() {
    this.controlNombre.value = null;
    this.controlFecha.value = null;
    this.botonCrear.classList.add('off');
  }

  validaForm() {
    return this.controlNombre.value.trim() !== '' && this.controlFecha.value.trim() !== '';
  }

  compruebaBotonCrear() {
    if (this.validaForm()) {
      this.botonCrear.classList.remove('off');
    } else {
      this.botonCrear.classList.add('off');
    }
  }

  filtraCursosFuturos() {
    return this.cursos.filter(curso => moment().isBefore(moment(curso.fecha)));
  }

  asignarHTML() {
    this.cursoBase = document.querySelector('.curso-base');
    this.listaCursos = document.querySelector('.cursos');
    this.loading = document.querySelector('.loading');
    this.checkCursosFuturos = document.querySelector('#cursos-futuros');
    this.botonAnyadir = document.querySelector('.icono-anyadir');
    this.botonVolver = document.querySelector('.icono-volver');
    this.toolBar = document.querySelector('.toolbar');
    this.botonCrear = document.querySelector('.boton-anyadir');
    this.controlNombre = document.querySelector('#nombre-formacion');
    this.controlFecha = document.querySelector('#fecha-formacion');
    this.formularioCrear = document.querySelector('form');
    this.mensaje = document.querySelector('.mensaje');
  }

  muestraMensaje(texto) {
    this.mensaje.textContent = texto;
    this.mensaje.classList.remove('off');
    setTimeout(() => {
      this.mensaje.classList.add('off');
    }, 3000);
  }

  activaDesactivaCargando(activar) {
    if (activar) {
      this.loading.classList.remove('off');
    } else {
      this.loading.classList.add('off');
    }
  }

  borraListaCursos() {
    while (this.listaCursos.firstElementChild) {
      this.listaCursos.removeChild(this.listaCursos.firstElementChild);
    }
  }

  pintaListaCursos() {
    this.borraListaCursos();
    this.cursos.sort((cursoA, cursoB) => {
      let fechaA = moment(cursoA.fecha);
      let fechaB = moment(cursoB.fecha);
      if (fechaA.isAfter(fechaB)) {
        return 1;
      } else if (fechaA.isBefore(fechaB)) {
        return -1;
      } else {
        return 0;
      }
    });
    for (let curso of this.cursos) {
      const fechaMoment = moment(curso.fecha);
      const nuevoLi = this.cursoBase.cloneNode(true);
      nuevoLi.classList.remove('off', 'curso-base');
      nuevoLi.querySelector('.curso-nombre').textContent = curso.nombre;
      nuevoLi.querySelector('.fecha').textContent = fechaMoment.format('DD-MM-YYYY');
      nuevoLi.addEventListener('mouseover', () => {
        nuevoLi.querySelector('.icono-borrar').classList.remove('off');
      });
      nuevoLi.addEventListener('mouseout', () => {
        nuevoLi.querySelector('.icono-borrar').classList.add('off');
      });
      nuevoLi.querySelector('.icono-borrar').addEventListener('click', () => {
        this.borrarCurborrarCurso(curso.id);
      });
      this.listaCursos.append(nuevoLi);
    }
  }

  borrarCurso(idCurso) {
    fetch('http://api.cursos.com/curso/' + idCurso, {
      method: 'DELETE'
    }).then(() => {
      this.cargaCursos();
      this.pintaListaCursos();
    });
  }

  inicializaDatepicker() {
    $('#fecha-formacion').datepicker({
      uiLibrary: 'bootstrap4',
      locale: 'es-es',
      format: 'dd-mm-yyyy',
      weekStartDay: 1,
      change: () => {
        this.compruebaBotonCrear();
      },
    });
  }
}

const cursos = new Cursos();

